#!/bin/bash

PRJ=$2
PRO=$3
ARGS=$3

DEV_PATH="/home/kerim/myfs/works/bilkon/filesystems/debian/armhf-stretch-devops/"

function qmake_qemu() {
	sudo /usr/sbin/chroot $DEV_PATH /root/qmake_qemu.sh $PRJ $PRO
}

function make_qemu() {
        sudo /usr/sbin/chroot $DEV_PATH /root/make_qemu.sh $PRJ $ARGS
}

function clean_qemu() {
        sudo /usr/sbin/chroot $DEV_PATH /root/clean_qemu.sh $PRJ
}

case "$1" in
        qmake)
            qmake_qemu
            ;;
         
        make)
            make_qemu
            ;;
        clean)
            clean_qemu
            ;;
         
        *)
            echo $"Usage: $0 {start|stop|restart|condrestart|status}"
            exit 1
 
esac
